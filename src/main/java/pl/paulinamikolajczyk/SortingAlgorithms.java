package pl.paulinamikolajczyk;

import java.util.ArrayList;
import java.util.List;

public class SortingAlgorithms {

    private ArrayList<Integer> integers;

    SortingAlgorithms(List<Integer> integers) {
        setIntegers((ArrayList<Integer>) integers);
    }

    public ArrayList<Integer> getIntegers() {
        return integers;
    }

    public void setIntegers(ArrayList<Integer> integers) {
        this.integers = integers;
    }

    public void sortBySelection() {

        int i, j, index = 0;

        for(i=0; i<integers.size(); i++) {
            int temp = integers.get(i);
            index = i;
            for(j=i+1; j<integers.size(); j++) {
                if(integers.get(j)<temp) {
                    temp = integers.get(j);
                    index = j;
                }
            }
            integers.set(index, integers.get(i));
            integers.set(i, temp);

        }

    }

}
