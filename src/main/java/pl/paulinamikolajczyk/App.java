package pl.paulinamikolajczyk;

import java.util.*;

/**
 * Hello world!
 *
 */
public class App<T>
{
    public static void main( String[] args )
    {

        System.out.println("**** Witaj w programie, w którym posortujesz wprowadzone dane ****");
        System.out.println("Tablica do posortowania: ");

        Integer[] temp = {8,2,9,6,1,7,10,4,5};
        List<Integer> tempList = Arrays.asList(temp);
        List<Integer> integerList = new ArrayList<Integer>(tempList);

        for (int x:integerList) {
            System.out.print(x + " ");
        }

        System.out.println("");

        System.out.println("Sortowanie przez wybieranie");
        SortingAlgorithms sa = new SortingAlgorithms(integerList);

        sa.sortBySelection();
        integerList = sa.getIntegers();

        System.out.println("");
        System.out.println("Tablica po sortowaniu: ");

        for (int x:integerList) {
            System.out.print(x + " ");
        }
    }

    private static ArrayList<Integer> setIntegerList(Scanner in) {

        ArrayList<Integer> integerList = new ArrayList<>();

        while(in.hasNext()) {
            integerList.add(in.nextInt());
        }
        return integerList;
    }

    private ArrayList<T> setInputList(Scanner in) {

        try {
            int number = in.nextInt();
            ArrayList<Integer> inputIntegerList = new ArrayList<>();
            inputIntegerList.add(number);
            while(in.hasNext()) {
                inputIntegerList.add(in.nextInt());
            }
            return (ArrayList<T>) inputIntegerList;
        }
        catch(InputMismatchException e) {
            ArrayList<String> inputStringList = new ArrayList<>();
            while(in.hasNext()) {
                inputStringList.add(in.next());
            }
            return (ArrayList<T>) inputStringList;

        }


    }
}



